def palindromechecker():
        checkpalin = raw_input("Enter string:")
        if (checkpalin == "") or (len(checkpalin) == 1) or (checkpalin.isalpha() == False):
                print("invalid input")
                palindromechecker()
        checkpalin = checkpalin.lower()
        lenpal = len(checkpalin)
        oddeven = lenpal % 2
        halfpal = lenpal // 2
        firstpart = (checkpalin[0:halfpal])
        secondpart = checkpalin[::-1][0:halfpal]
        if (firstpart == secondpart):
                if (oddeven == 0):
                        print("odd palindrome")
                        palindromechecker()
                else:
                        print("palindrome")
                        palindromechecker()
palindromechecker()
