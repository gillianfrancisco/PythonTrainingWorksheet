class cartDB:
    def __init__(self, conn):
        self.conn = conn

    def getCart(self, user):
        return self.conn.find({'username':user})

    def checkCart(self, item_name, price, user):
        return self.conn.find_one({'username':user, 'item_name':item_name, 'price': price})
     
    def addCart(self, item_name, price, user, quantity, flag=0, obj=0):
    	from bson.objectid import ObjectId
    	item_total = float(price) * float(quantity)
    	if flag == 0:
            self.conn.insert({'username':user, 'item_name':item_name, 'price': price, 'quantity':quantity, 'item_total':item_total})
        else:
            self.conn.update({'_id':ObjectId(obj)},{'username':user, 'item_name':item_name, 'price': price, 'quantity':quantity, 'item_total':item_total})
