import functools
from collections import deque
import os
import time
food = []
ordername = deque([])   
    
def pricetotal(a,b,c,d):
    return 50*a,30*b,55*c,40*d

def payout(ordername,food):
    burger = 0
    fries = 0
    tapsilog = 0
    siopao = 0
    print("\n") 
    print(ordername.popleft()),
    print(": order payout")
    for foodorder in food:
        if (foodorder == "burger"):
            burger = burger +1
        elif(foodorder == "fries"):
            fries = fries +1
        elif(foodorder == "tapsilog"):
            tapsilog = tapsilog + 1
        elif(foodorder == "siopao"):
            siopao = siopao + 1
    price = pricetotal(burger,fries,tapsilog,siopao)
    print('Burger -',burger)
    print('Fries - ',fries)
    print('Tapsilog -',tapsilog)
    print('Siopao -',siopao)
    totalprice = ("php"+ str(sum(price)) +".00")
    print(totalprice)
    tprice = int(sum(price))
    paying = int(input("Enter pay:"))
    change = paying - tprice
    print("recieved",paying)
    print("change",change)
    food = []
    mainmenu(ordername,food)

def checkingbill(ordername,food):
    burger = 0
    fries = 0
    tapsilog = 0
    siopao = 0
    for foodorder in food:
        if (foodorder == "burger"):
            burger = burger +1
        elif(foodorder == "fries"):
            fries = fries +1
        elif(foodorder == "tapsilog"):
            tapsilog = tapsilog + 1
        elif(foodorder == "siopao"):
            siopao = siopao + 1
    price = pricetotal(burger,fries,tapsilog,siopao)
    print('Burger -',burger)
    print('Fries - ',fries)
    print('Tapsilog -',tapsilog)
    print('Siopao -',siopao)
    totalprice = ("php"+ str(sum(price)) +".00")
    print(totalprice)
    mainmenu(ordername,food)

def neworder(ordername,food):
    reply = "y"
    ordernaming = raw_input("Enter name of client:")
    ordername.append(ordernaming)
    while (reply == "y"):
        print("\n[1]Burger \n[2]Fries \n[3]Tapsilog \n[4]Siopao")
        neworder = (raw_input("Enter code for food ordered or [del] to delete last ordered item: "))
        if (neworder == "1"):
            food.append("burger")
            print("burger has been added")
        elif (neworder == "2"):
            food.append("fries")
            print("fries has been added")
        elif (neworder == "3"):
            food.append("tapsilog")
            print("tapsilog has been added")
        elif(neworder == "4"):
            food.append("siopao")
            print("siopao has been added")
        elif (neworder == "del"):
            print(food.pop()),
            print("has been deleted from the order list")
        reply = raw_input("order another? (y/n): ")
    mainmenu(ordername,food)

def mainmenu(ordername,food):
    print("\n[1]New order\n[2]Check bill\n[3]Payout")
    choice = int(input("Enter choice: "))
    if (choice == 1):
        neworder(ordername,food)
    elif(choice==2):
        checkingbill(ordername,food)
    elif(choice==3):
        payout(ordername,food)
    else:
        print("\nIncorrect input")
        mainmenu(ordername,food) 


        
mainmenu(ordername,food)
